defmodule Rummage.Ecto.CustomHooks.SearchInTimestamps do
  @moduledoc """
  `Rummage.Ecto.CustomHooks.SearchInTimestamps` позволяет искать по полям
   inserted_at и updated_at учитывая только дату, без времени.

   Основан на `Rummage.Ecto.Hooks.Search`.

   Разработан с целью добавления возможности поиска записей с использованием стандартного
   датапикера.

   Для использования данной возможности нужно добавить в параметры вызова Rummage.Ecto.rummage опцию
   `search: Rummage.Ecto.CustomHooks.SearchInTimestamps`, например:
   `{queryable, rummage} = Rummage.Ecto.rummage(TargetGroup, rummage, search: Rummage.Ecto.CustomHooks.SearchInTimestamps)`
  """

  import Ecto.Query

  alias Rummage.Ecto.Services.BuildSearchQuery

  @behaviour Rummage.Ecto.Hook

  @search_types ~w(like ilike eq gt lt gteq lteq in)

  @doc """
  Builds a search queryable on top of the given `queryable` from the rummage parameters
  from the given `rummage` struct.

  ## Examples
  When rummage struct passed doesn't have the key "search", it simply returns the
  queryable itself:

      iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
      iex> import Ecto.Query
      iex> Search.run(Parent, %{})
      Parent

  When the queryable passed is not just a struct:

      iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
      iex> import Ecto.Query
      iex> queryable = from u in "parents"
      #Ecto.Query<from p in "parents">
      iex>  Search.run(queryable, %{})
      #Ecto.Query<from p in "parents">

  When rummage `struct` passed has the key `"search"`, but with a value of `%{}`, `""`
  or `[]` it simply returns the `queryable` itself:

      iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
      iex> import Ecto.Query
      iex> Search.run(Parent, %{"search" => %{}})
      Parent

      iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
      iex> import Ecto.Query
      iex> Search.run(Parent, %{"search" => ""})
      Parent

      iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
      iex> import Ecto.Query
      iex> Search.run(Parent, %{"search" => %{}})
      Parent

  When rummage `struct` passed has the key "search", with `field`, `associations`
  `search_type` and `term` it returns a searched version of the `queryable` passed in
  as the argument:

  When `associations` is an empty `list`:
    When rummage `struct` passed has `search_type` of `like`, it returns
    a searched version of the `queryable` with `like` search query:

        iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "like", "search_term" => "field_!"}}}
        %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "like", "search_term" => "field_!"}}}
        iex> queryable = from u in "parents"
        #Ecto.Query<from p in "parents">
        iex> Search.run(queryable, rummage)
        #Ecto.Query<from p in subquery(from p in "parents"), where: like(p.field_1, ^"%field_!%")>

    When rummage `struct` passed has `search_type` of `ilike` (case insensitive), it returns
    a searched version of the `queryable` with `ilike` search query:

        iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "ilike", "search_term" => "field_!"}}}
        %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "ilike", "search_term" => "field_!"}}}
        iex> queryable = from u in "parents"
        #Ecto.Query<from p in "parents">
        iex> Search.run(queryable, rummage)
        #Ecto.Query<from p in subquery(from p in "parents"), where: ilike(p.field_1, ^"%field_!%")>

    When rummage `struct` passed has `search_type` of `eq`, it returns
    a searched version of the `queryable` with `==` search query:

        iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_!"}}}
        %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_!"}}}
        iex> queryable = from u in "parents"
        #Ecto.Query<from p in "parents">
        iex> Search.run(queryable, rummage)
        #Ecto.Query<from p in subquery(from p in "parents"), where: p.field_1 == ^"field_!">

    When rummage `struct` passed has `search_type` of `gt`, it returns
    a searched version of the `queryable` with `>` search query:

        iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "gt", "search_term" => "field_!"}}}
        %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "gt", "search_term" => "field_!"}}}
        iex> queryable = from u in "parents"
        #Ecto.Query<from p in "parents">
        iex> Search.run(queryable, rummage)
        #Ecto.Query<from p in subquery(from p in "parents"), where: p.field_1 > ^"field_!">

    When rummage `struct` passed has `search_type` of `lt`, it returns
    a searched version of the `queryable` with `<` search query:

        iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "lt", "search_term" => "field_!"}}}
        %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "lt", "search_term" => "field_!"}}}
        iex> queryable = from u in "parents"
        #Ecto.Query<from p in "parents">
        iex> Search.run(queryable, rummage)
        #Ecto.Query<from p in subquery(from p in "parents"), where: p.field_1 < ^"field_!">

    When rummage `struct` passed has `search_type` of `gteq`, it returns
    a searched version of the `queryable` with `>=` search query:

        iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "gteq", "search_term" => "field_!"}}}
        %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "gteq", "search_term" => "field_!"}}}
        iex> queryable = from u in "parents"
        #Ecto.Query<from p in "parents">
        iex> Search.run(queryable, rummage)
        #Ecto.Query<from p in subquery(from p in "parents"), where: p.field_1 >= ^"field_!">

    When rummage `struct` passed has `search_type` of `lteq`, it returns
    a searched version of the `queryable` with `<=` search query:

        iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "lteq", "search_term" => "field_!"}}}
        %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "lteq", "search_term" => "field_!"}}}
        iex> queryable = from u in "parents"
        #Ecto.Query<from p in "parents">
        iex> Search.run(queryable, rummage)
        #Ecto.Query<from p in subquery(from p in "parents"), where: p.field_1 <= ^"field_!">

  When `associations` is not an empty `list`:
    When rummage `struct` passed has `search_type` of `like`, it returns
    a searched version of the `queryable` with `like` search query:

        iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{"field_1" => %{"assoc" => ["parent", "parent"], "search_type" => "like", "search_term" => "field_!"}}}
        %{"search" => %{"field_1" => %{"assoc" => ["parent", "parent"], "search_type" => "like", "search_term" => "field_!"}}}
        iex> queryable = from u in "parents"
        #Ecto.Query<from p in "parents">
        iex> Search.run(queryable, rummage)
        #Ecto.Query<from p0 in subquery(from p in "parents"), join: p1 in assoc(p0, :parent), join: p2 in assoc(p1, :parent), where: like(p2.field_1, ^"%field_!%")>

    When rummage `struct` passed has `search_type` of `lteq`, it returns
    a searched version of the `queryable` with `<=` search query:

        iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{"field_1" => %{"assoc" => ["parent", "parent"], "search_type" => "lteq", "search_term" => "field_!"}}}
        %{"search" => %{"field_1" => %{"assoc" => ["parent", "parent"], "search_type" => "lteq", "search_term" => "field_!"}}}
        iex> queryable = from u in "parents"
        #Ecto.Query<from p in "parents">
        iex> Search.run(queryable, rummage)
        #Ecto.Query<from p0 in subquery(from p in "parents"), join: p1 in assoc(p0, :parent), join: p2 in assoc(p1, :parent), where: p2.field_1 <= ^"field_!">

    When rummage `struct` passed has an empty string as `search_term`, it returns the `queryable` itself:

        iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{"field_1" => %{"assoc" => ["parent", "parent"], "search_type" => "lteq", "search_term" => ""}}}
        %{"search" => %{"field_1" => %{"assoc" => ["parent", "parent"], "search_type" => "lteq", "search_term" => ""}}}
        iex> queryable = from u in "parents"
        #Ecto.Query<from p in "parents">
        iex> Search.run(queryable, rummage)
        #Ecto.Query<from p in "parents">

    When rummage `struct` passed has nil as `search_term`, it returns the `queryable` itself:

        iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{"field_1" => %{"assoc" => ["parent", "parent"], "search_type" => "lteq", "search_term" => nil}}}
        %{"search" => %{"field_1" => %{"assoc" => ["parent", "parent"], "search_type" => "lteq", "search_term" => nil}}}
        iex> queryable = from u in "parents"
        #Ecto.Query<from p in "parents">
        iex> Search.run(queryable, rummage)
        #Ecto.Query<from p in "parents">

    When rummage `struct` passed has an empty array as `search_term`, it returns the `queryable` itself:

        iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{"field_1" => %{"assoc" => ["parent", "parent"], "search_type" => "lteq", "search_term" => []}}}
        %{"search" => %{"field_1" => %{"assoc" => ["parent", "parent"], "search_type" => "lteq", "search_term" => []}}}
        iex> queryable = from u in "parents"
        #Ecto.Query<from p in "parents">
        iex> Search.run(queryable, rummage)
        #Ecto.Query<from p in "parents">

  When `associations` is an empty `string`:
    When rummage `struct` passed has `search_type` of `like`, it returns
    a searched version of the `queryable` with `like` search query:

        iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{"field_1" => %{"assoc" => "", "search_type" => "like", "search_term" => "field_!"}}}
        %{"search" => %{"field_1" => %{"assoc" => "", "search_type" => "like", "search_term" => "field_!"}}}
        iex> queryable = from u in "parents"
        #Ecto.Query<from p in "parents">
        iex> Search.run(queryable, rummage)
        #Ecto.Query<from p in subquery(from p in "parents"), where: like(p.field_1, ^"%field_!%")>
  """
  @spec run(Ecto.Query.t(), map) :: {Ecto.Query.t(), map}
  def run(queryable, rummage) do
    search_params = Map.get(rummage, "search")

    case search_params do
      a when a in [nil, [], {}, ""] -> queryable
      _ -> handle_search(queryable, search_params)
    end
  end

  @doc """
  Implementation of `before_hook` for `Rummage.Ecto.CustomHooks.SearchInTimestamps`.
  This just returns back `rummage` at this point.
  It doesn't matter what `queryable` or `opts` are, it just returns back `rummage`.

  ## Examples
      iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
      iex> SearchInTimestamps.before_hook(Parent, %{}, %{})
      %{}
  """
  @spec before_hook(Ecto.Query.t(), map, map) :: map
  def before_hook(_queryable, rummage, _opts), do: rummage

  defp handle_search(queryable, search_params) do
    search_params
    |> Map.to_list()
    |> Enum.reduce(queryable, &search_queryable(&1, &2))
  end

  @spec search_queryable({String.t(), String.t()}, Ecto.Query.t()) :: Ecto.Query.t()
  defp search_queryable(param, queryable) do
    field =
      param
      |> elem(0)
      |> String.to_atom()

    field_params =
      param
      |> elem(1)

    association_names =
      case field_params["assoc"] do
        a when a in [nil, "", []] -> []
        assoc -> assoc
      end

    search_type = field_params["search_type"]
    search_term = field_params["search_term"]

    case search_term do
      s when s in [nil, "", []] ->
        queryable

      _ ->
        queryable = from(e in subquery(queryable))

        queryable = Enum.reduce(association_names, queryable, &join_by_association(&1, &2))

        queryable =
          if field in [:inserted_at, :updated_at] do
            build_query(queryable, field, search_type, search_term)
          else
            BuildSearchQuery.run(queryable, field, search_type, search_term)
          end

        queryable
    end
  end

  @spec join_by_association(String.t(), Ecto.Query.t()) :: Ecto.Query.t()
  defp join_by_association(association, queryable) do
    join(queryable, :inner, [..., p1], p2 in assoc(p1, ^String.to_atom(association)))
  end

  defp build_query(queryable, field, search_type, search_term) do
    if Enum.member?(@search_types, search_type) do
      apply(__MODULE__, String.to_atom("handle_#{search_type}"), [queryable, field, search_term])
    else
      queryable
    end
  end

  @doc """
  Builds a searched `queryable` on top of the given `queryable` using `field` and `search_type`
  when the `search_type` is `eq`.

  ## Examples

      iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
      iex> import Ecto.Query
      iex> queryable = from u in "target_groups"
      #Ecto.Query<from p in "target_groups">
      iex> SearchInTimestamps.handle_eq(queryable, :inserted_at, Frontix.Utils.parse_date("01.04.2020"))
      #Ecto.Query<from t0 in "target_groups", where: fragment("date_trunc('day', ? at time zone 'UTC' at time zone 'Europe/Moscow')", t0.inserted_at) == ^#DateTime<2020-04-01 00:00:00+03:00 MSK Europe/Moscow>>
  """
  @spec handle_eq(Ecto.Query.t(), atom(), any()) :: {Ecto.Query.t()}
  def handle_eq(queryable, field, search_term) do
    queryable
    |> where(
      [..., b],
      fragment(
        "date_trunc('day', ? at time zone 'UTC' at time zone 'Europe/Moscow')",
        field(b, ^field)
      ) == ^search_term
    )
  end

  # gt(lt(gteq(lteq)))

  @doc """
  Builds a searched `queryable` on top of the given `queryable` using `field` and `search_type`
  when the `search_type` is `lt`.

  ## Examples

      iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
      iex> import Ecto.Query
      iex> queryable = from u in "target_groups"
      #Ecto.Query<from p in "target_groups">
      iex> SearchInTimestamps.handle_lt(queryable, :inserted_at, Frontix.Utils.parse_date("01.04.2020"))
      #Ecto.Query<from t0 in "target_groups", where: fragment("date_trunc('day', ? at time zone 'UTC' at time zone 'Europe/Moscow')", t0.inserted_at) < ^#DateTime<2020-04-01 00:00:00+03:00 MSK Europe/Moscow>>
  """
  @spec handle_lt(Ecto.Query.t(), atom(), any()) :: {Ecto.Query.t()}
  def handle_lt(queryable, field, search_term) do
    queryable
    |> where(
      [..., b],
      fragment(
        "date_trunc('day', ? at time zone 'UTC' at time zone 'Europe/Moscow')",
        field(b, ^field)
      ) < ^search_term
    )
  end

  @doc """
  Builds a searched `queryable` on top of the given `queryable` using `field` and `search_type`
  when the `search_type` is `gt`.

  ## Examples

      iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
      iex> import Ecto.Query
      iex> queryable = from u in "target_groups"
      #Ecto.Query<from p in "target_groups">
      iex> SearchInTimestamps.handle_gt(queryable, :inserted_at, Frontix.Utils.parse_date("01.04.2020"))
      #Ecto.Query<from t0 in "target_groups", where: fragment("date_trunc('day', ? at time zone 'UTC' at time zone 'Europe/Moscow')", t0.inserted_at) > ^#DateTime<2020-04-01 00:00:00+03:00 MSK Europe/Moscow>>
  """
  @spec handle_gt(Ecto.Query.t(), atom(), any()) :: {Ecto.Query.t()}
  def handle_gt(queryable, field, search_term) do
    queryable
    |> where(
      [..., b],
      fragment(
        "date_trunc('day', ? at time zone 'UTC' at time zone 'Europe/Moscow')",
        field(b, ^field)
      ) > ^search_term
    )
  end

  @doc """
  Builds a searched `queryable` on top of the given `queryable` using `field` and `search_type`
  when the `search_type` is `lteq`.

  ## Examples

      iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
      iex> import Ecto.Query
      iex> queryable = from u in "target_groups"
      #Ecto.Query<from p in "target_groups">
      iex> SearchInTimestamps.handle_lteq(queryable, :inserted_at, Frontix.Utils.parse_date("01.04.2020"))
      #Ecto.Query<from t0 in "target_groups", where: fragment("date_trunc('day', ? at time zone 'UTC' at time zone 'Europe/Moscow')", t0.inserted_at) <= ^#DateTime<2020-04-01 00:00:00+03:00 MSK Europe/Moscow>>
  """
  @spec handle_lteq(Ecto.Query.t(), atom(), any()) :: {Ecto.Query.t()}
  def handle_lteq(queryable, field, search_term) do
    queryable
    |> where(
      [..., b],
      fragment(
        "date_trunc('day', ? at time zone 'UTC' at time zone 'Europe/Moscow')",
        field(b, ^field)
      ) < ^search_term
    )
  end

  @doc """
  Builds a searched `queryable` on top of the given `queryable` using `field` and `search_type`
  when the `search_type` is `gteq`.

  ## Examples

      iex> alias Rummage.Ecto.CustomHooks.SearchInTimestamps
      iex> import Ecto.Query
      iex> queryable = from u in "target_groups"
      #Ecto.Query<from p in "target_groups">
      iex> SearchInTimestamps.handle_gteq(queryable, :inserted_at, Frontix.Utils.parse_date("01.04.2020"))
      #Ecto.Query<from t0 in "target_groups", where: fragment("date_trunc('day', ? at time zone 'UTC' at time zone 'Europe/Moscow')", t0.inserted_at) >= ^#DateTime<2020-04-01 00:00:00+03:00 MSK Europe/Moscow>>
  """
  @spec handle_gteq(Ecto.Query.t(), atom(), any()) :: {Ecto.Query.t()}
  def handle_gteq(queryable, field, search_term) do
    queryable
    |> where(
      [..., b],
      fragment(
        "date_trunc('day', ? at time zone 'UTC' at time zone 'Europe/Moscow')",
        field(b, ^field)
      ) >= ^search_term
    )
  end
end
