defmodule Rummage.Ecto.CustomHooks.DisjunctiveSearch do
  @moduledoc """
  Хук для Rummage, в котором поля при поиске сочетаются через логическое "или", а не "и".

  Основан на `Rummage.Ecto.Hooks.Search`.

  При этом можно задать поля, которые должны быть объединены между собой логическим "и" - то есть,
  можно сформировать, например, такой поисковый запрос:
  "имя И отчество И фамилия ИЛИ емейл ИЛИ серия И номер паспорта".

  Для этого нужно при вызове хука передать в rummage параметр conjunctions
  со списком всех таких групп.

  Например, для указанного выше поискового запроса conjunctions будет выглядеть как:
  [[:first_name, :middle_name, :last_name], [:passport_series, :passport_number]]

  Если параметр conjunctions не задан, или в нем пустой список,
  то все поисковые поля (непустые, конечно) сочетаются через "или".

  **Не** поддерживается поиск по связанным сущностям, т.е. параметр assoc напрочь игнорируется.

  Usage:
  For a regular search:

  This returns a `queryable` which upon running will give a list of `Parent`(s)
  searched by ascending `field_1`

  ```elixir
  alias Rummage.Ecto.CustomHooks.DisjunctiveSearch

  searched_queryable = Search.run(Parent, %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "like", "search_term" => "field_!"}}})

  ```

  For a case-insensitive search:

  This returns a `queryable` which upon running will give a list of `Parent`(s)
  searched by ascending case insensitive `field_1`.

  Keep in mind that `case_insensitive` can only be called for `text` fields

  ```elixir
  alias Rummage.Ecto.CustomHooks.DisjunctiveSearch

  searched_queryable = Search.run(Parent, %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "ilike", "search_term" => "field_!"}}})

  ```

  There are many other `search_types`. Check out `Rummage.Ecto.Services.BuildSearchQuery`'s docs
  to explore more `search_types`

  This module can be overridden with a custom module while using `Rummage.Ecto`
  in `Ecto` struct module:

  In the `Ecto` module:
  ```elixir
  Rummage.Ecto.rummage(queryable, rummage, search: CustomHook)
  ```

  OR

  Globally for all models in `config.exs`:
  ```elixir
  config :rummage_ecto,
    Rummage.Ecto,
    default_search: CustomHook
  ```

  The `CustomHook` must implement `behaviour `Rummage.Ecto.Hook`. For examples of `CustomHook`, check out some
    `custom_hooks` that are shipped with elixir: `Rummage.Ecto.CustomHooks.SimpleSearch`, `Rummage.Ecto.CustomHooks.SimpleSort`,
    Rummage.Ecto.CustomHooks.SimplePaginate
  """

  import Ecto.Query

  @behaviour Rummage.Ecto.Hook

  @doc """
  Builds a search queryable on top of the given `queryable` from the rummage parameters
  from the given `rummage` struct.

  **Поля при поиске сочетаются через логическое "или", а не "и".**

  При этом можно задать поля, которые должны быть объединены между собой логическим "и" - то есть,
  можно сформировать, например, такой поисковый запрос:
  "имя И отчество И фамилия ИЛИ емейл ИЛИ серия И номер паспорта".

  Для этого нужно при вызове хука передать в rummage параметр conjunctions
  со списком всех таких групп.

  Например, для указанного выше поискового запроса conjunctions будет выглядеть как:
  [[:first_name, :middle_name, :last_name], [:passport_series, :passport_number]]

  Если параметр conjunctions не задан, или в нем пустой список,
  то все поисковые поля (непустые, конечно) сочетаются через "или".

  **Не** поддерживается поиск по связанным сущностям, т.е. параметр assoc напрочь игнорируется.

  ## Примеры

  Совсем без параметра conjunctions в rummage:

        iex> alias Rummage.Ecto.CustomHooks.DisjunctiveSearch
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_!"}}}
        %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_!"}}}
        iex> queryable = from u in "parents"
        iex> DisjunctiveSearch.run(queryable, rummage)
        #Ecto.Query<from p0 in "parents", or_where: true and p0.field_1 == ^"field_!">

  Пустой список в параметре conjunctions (то же самое, что и без этого параметра):

        iex> alias Rummage.Ecto.CustomHooks.DisjunctiveSearch
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_!"}},\
          "conjunctions" => []\
        }
        %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_!"}}, "conjunctions" => []}
        iex> queryable = from u in "parents"
        iex> DisjunctiveSearch.run(queryable, rummage)
        #Ecto.Query<from p0 in "parents", or_where: true and p0.field_1 == ^"field_!">

  Одна группа параметров в conjunctions - запрос вида field_1 И field_2 ИЛИ field_3:

        iex> alias Rummage.Ecto.CustomHooks.DisjunctiveSearch
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{\
          "field_1" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_1_term"},\
          "field_2" => %{"assoc" => [], "search_type" => "gt", "search_term" => "field_2_term"},\
          "field_3" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_3_term"},\
          },\
          "conjunctions" => [[:field_1, :field_2]]\
        }
        %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_1_term"}, "field_2" => %{"assoc" => [], "search_type" => "gt", "search_term" => "field_2_term"}, "field_3" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_3_term"}}, "conjunctions" => [[:field_1, :field_2]]}
        iex> queryable = from u in "parents"
        iex> DisjunctiveSearch.run(queryable, rummage)
        #Ecto.Query<from p0 in "parents", or_where: true and p0.field_1 == ^"field_1_term" and p0.field_2 > ^"field_2_term", or_where: true and p0.field_3 == ^"field_3_term">

  Две группы параметров в conjunctions - запрос вида field_1 И field_2 ИЛИ field_3 ИЛИ field_4 И field_5:

        iex> alias Rummage.Ecto.CustomHooks.DisjunctiveSearch
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{\
          "field_1" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_1_term"},\
          "field_2" => %{"assoc" => [], "search_type" => "gt", "search_term" => "field_2_term"},\
          "field_3" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_3_term"},\
          "field_4" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_4_term"},\
          "field_5" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_5_term"},\
          },\
          "conjunctions" => [[:field_1, :field_2], [:field_4, :field_5]]\
        }
        %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_1_term"}, "field_2" => %{"assoc" => [], "search_type" => "gt", "search_term" => "field_2_term"}, "field_3" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_3_term"}, "field_4" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_4_term"}, "field_5" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_5_term"}}, "conjunctions" => [[:field_1, :field_2], [:field_4, :field_5]]}
        iex> queryable = from u in "parents"
        iex> DisjunctiveSearch.run(queryable, rummage)
        #Ecto.Query<from p0 in "parents", or_where: true and p0.field_1 == ^"field_1_term" and p0.field_2 > ^"field_2_term", or_where: true and p0.field_4 == ^"field_4_term" and p0.field_5 == ^"field_5_term", or_where: true and p0.field_3 == ^"field_3_term">

  Две группы параметров в conjunctions, но в поисковом запросе заданы значения только для полей из первой группы:

        iex> alias Rummage.Ecto.CustomHooks.DisjunctiveSearch
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{\
          "field_1" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_1_term"},\
          "field_2" => %{"assoc" => [], "search_type" => "gt", "search_term" => "field_2_term"},\
          "field_3" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_3_term"},\
          "field_4" => %{"assoc" => [], "search_type" => "eq", "search_term" => ""},\
          "field_5" => %{"assoc" => [], "search_type" => "eq", "search_term" => ""},\
          },\
          "conjunctions" => [[:field_1, :field_2], [:field_4, :field_5]]\
        }
        %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_1_term"}, "field_2" => %{"assoc" => [], "search_type" => "gt", "search_term" => "field_2_term"}, "field_3" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_3_term"}, "field_4" => %{"assoc" => [], "search_type" => "eq", "search_term" => ""}, "field_5" => %{"assoc" => [], "search_type" => "eq", "search_term" => ""}}, "conjunctions" => [[:field_1, :field_2], [:field_4, :field_5]]}
        iex> queryable = from u in "parents"
        iex> DisjunctiveSearch.run(queryable, rummage)
        #Ecto.Query<from p0 in "parents", or_where: true and p0.field_1 == ^"field_1_term" and p0.field_2 > ^"field_2_term", or_where: true and p0.field_3 == ^"field_3_term">

  Две группы параметров в conjunctions, но в поисковом запросе есть только по одному полю каждой группы:

        iex> alias Rummage.Ecto.CustomHooks.DisjunctiveSearch
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{\
          "field_1" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_1_term"},\
          "field_2" => %{"assoc" => [], "search_type" => "gt", "search_term" => ""},\
          "field_3" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_3_term"},\
          "field_4" => %{"assoc" => [], "search_type" => "eq", "search_term" => ""},\
          "field_5" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_5_term"},\
          },\
          "conjunctions" => [[:field_1, :field_2], [:field_4, :field_5]]\
        }
        %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_1_term"}, "field_2" => %{"assoc" => [], "search_type" => "gt", "search_term" => ""}, "field_3" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_3_term"}, "field_4" => %{"assoc" => [], "search_type" => "eq", "search_term" => ""}, "field_5" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_5_term"}}, "conjunctions" => [[:field_1, :field_2], [:field_4, :field_5]]}
        iex> queryable = from u in "parents"
        iex> DisjunctiveSearch.run(queryable, rummage)
        #Ecto.Query<from p0 in "parents", or_where: true and p0.field_1 == ^"field_1_term" and ^true, or_where: true and ^true and p0.field_5 == ^"field_5_term", or_where: true and p0.field_3 == ^"field_3_term">

  Одна группа параметров в conjunctions и несколько одиночных параметров:

        iex> alias Rummage.Ecto.CustomHooks.DisjunctiveSearch
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{\
          "field_1" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_1_term"},\
          "field_2" => %{"assoc" => [], "search_type" => "gt", "search_term" => "field_2_term"},\
          "field_3" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_3_term"},\
          "field_4" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_4_term"},\
          "field_5" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_5_term"},\
          },\
          "conjunctions" => [[:field_3, :field_4]]\
        }
        %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_1_term"}, "field_2" => %{"assoc" => [], "search_type" => "gt", "search_term" => "field_2_term"}, "field_3" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_3_term"}, "field_4" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_4_term"}, "field_5" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_5_term"}}, "conjunctions" => [[:field_3, :field_4]]}
        iex> queryable = from u in "parents"
        iex> DisjunctiveSearch.run(queryable, rummage)
        #Ecto.Query<from p0 in "parents", or_where: true and p0.field_3 == ^"field_3_term" and p0.field_4 == ^"field_4_term", or_where: true and p0.field_1 == ^"field_1_term", or_where: true and p0.field_2 > ^"field_2_term", or_where: true and p0.field_5 == ^"field_5_term">


  ## Examples
  When rummage struct passed doesn't have the key "search", it simply returns the
  queryable itself:

      iex> alias Rummage.Ecto.CustomHooks.DisjunctiveSearch
      iex> import Ecto.Query
      iex> DisjunctiveSearch.run(Parent, %{})
      Parent

  When the queryable passed is not just a struct:

      iex> alias Rummage.Ecto.CustomHooks.DisjunctiveSearch
      iex> import Ecto.Query
      iex> queryable = from u in "parents"
      #Ecto.Query<from p0 in "parents">
      iex>  DisjunctiveSearch.run(queryable, %{})
      #Ecto.Query<from p0 in "parents">

  When rummage `struct` passed has the key `"search"`, but with a value of `%{}`, `""`
  or `[]` it simply returns the `queryable` itself:

      iex> alias Rummage.Ecto.CustomHooks.DisjunctiveSearch
      iex> import Ecto.Query
      iex> DisjunctiveSearch.run(Parent, %{"search" => %{}})
      Parent

      iex> alias Rummage.Ecto.CustomHooks.DisjunctiveSearch
      iex> import Ecto.Query
      iex> DisjunctiveSearch.run(Parent, %{"search" => ""})
      Parent

      iex> alias Rummage.Ecto.CustomHooks.DisjunctiveSearch
      iex> import Ecto.Query
      iex> DisjunctiveSearch.run(Parent, %{"search" => %{}})
      Parent

  When rummage `struct` passed has the key "search", with `field`, `associations`
  `search_type` and `term` it returns a searched version of the `queryable` passed in
  as the argument:

    When rummage `struct` passed has `search_type` of `like`, it returns
    a searched version of the `queryable` with `like` search query:

        iex> alias Rummage.Ecto.CustomHooks.DisjunctiveSearch
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "like", "search_term" => "field_!"}}}
        %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "like", "search_term" => "field_!"}}}
        iex> queryable = from u in "parents"
        #Ecto.Query<from p0 in "parents">
        iex> DisjunctiveSearch.run(queryable, rummage)
        #Ecto.Query<from p0 in "parents", or_where: true and like(p0.field_1, ^"%field_!%")>

    When rummage `struct` passed has `search_type` of `ilike` (case insensitive), it returns
    a searched version of the `queryable` with `ilike` search query:

        iex> alias Rummage.Ecto.CustomHooks.DisjunctiveSearch
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "ilike", "search_term" => "field_!"}}}
        %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "ilike", "search_term" => "field_!"}}}
        iex> queryable = from u in "parents"
        #Ecto.Query<from p0 in "parents">
        iex> DisjunctiveSearch.run(queryable, rummage)
        #Ecto.Query<from p0 in "parents", or_where: true and ilike(p0.field_1, ^"%field_!%")>

    When rummage `struct` passed has `search_type` of `eq`, it returns
    a searched version of the `queryable` with `==` search query:

        iex> alias Rummage.Ecto.CustomHooks.DisjunctiveSearch
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_!"}}}
        %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "eq", "search_term" => "field_!"}}}
        iex> queryable = from u in "parents"
        #Ecto.Query<from p0 in "parents">
        iex> DisjunctiveSearch.run(queryable, rummage)
        #Ecto.Query<from p0 in "parents", or_where: true and p0.field_1 == ^"field_!">

    When rummage `struct` passed has `search_type` of `gt`, it returns
    a searched version of the `queryable` with `>` search query:

        iex> alias Rummage.Ecto.CustomHooks.DisjunctiveSearch
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "gt", "search_term" => "field_!"}}}
        %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "gt", "search_term" => "field_!"}}}
        iex> queryable = from u in "parents"
        #Ecto.Query<from p0 in "parents">
        iex> DisjunctiveSearch.run(queryable, rummage)
        #Ecto.Query<from p0 in "parents", or_where: true and p0.field_1 > ^"field_!">

    When rummage `struct` passed has `search_type` of `lt`, it returns
    a searched version of the `queryable` with `<` search query:

        iex> alias Rummage.Ecto.CustomHooks.DisjunctiveSearch
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "lt", "search_term" => "field_!"}}}
        %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "lt", "search_term" => "field_!"}}}
        iex> queryable = from u in "parents"
        #Ecto.Query<from p0 in "parents">
        iex> DisjunctiveSearch.run(queryable, rummage)
        #Ecto.Query<from p0 in "parents", or_where: true and p0.field_1 < ^"field_!">

    When rummage `struct` passed has `search_type` of `gteq`, it returns
    a searched version of the `queryable` with `>=` search query:

        iex> alias Rummage.Ecto.CustomHooks.DisjunctiveSearch
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "gteq", "search_term" => "field_!"}}}
        %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "gteq", "search_term" => "field_!"}}}
        iex> queryable = from u in "parents"
        #Ecto.Query<from p0 in "parents">
        iex> DisjunctiveSearch.run(queryable, rummage)
        #Ecto.Query<from p0 in "parents", or_where: true and p0.field_1 >= ^"field_!">

    When rummage `struct` passed has `search_type` of `lteq`, it returns
    a searched version of the `queryable` with `<=` search query:

        iex> alias Rummage.Ecto.CustomHooks.DisjunctiveSearch
        iex> import Ecto.Query
        iex> rummage = %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "lteq", "search_term" => "field_!"}}}
        %{"search" => %{"field_1" => %{"assoc" => [], "search_type" => "lteq", "search_term" => "field_!"}}}
        iex> queryable = from u in "parents"
        #Ecto.Query<from p0 in "parents">
        iex> DisjunctiveSearch.run(queryable, rummage)
        #Ecto.Query<from p0 in "parents", or_where: true and p0.field_1 <= ^"field_!">
  """
  @spec run(Ecto.Query.t, map) :: {Ecto.Query.t, map}
  def run(queryable, rummage) do
    search_params = Map.get(rummage, "search")
    conjunctions = Map.get(rummage, "conjunctions") || []

    case search_params do
      a when a in [nil, [], {}, [""], "", %{}] ->
        queryable

      _ ->
        handle_search(from(s in queryable), search_params, conjunctions)
    end
  end

  @doc """
  Implementation of `before_hook` for `Rummage.Ecto.Hooks.Search`. This just returns back `rummage` at this point.
  It doesn't matter what `queryable` or `opts` are, it just returns back `rummage`.

  ## Examples
      iex> alias Rummage.Ecto.Hooks.Search
      iex> Search.before_hook(Parent, %{}, %{})
      %{}
  """
  @spec before_hook(Ecto.Query.t, map, map) :: map
  def before_hook(_queryable, rummage, _opts), do: rummage

  defp handle_search(queryable, search_params, conjunctions) do
    # Найдем "одиночные" параметры поиска - т.е. те, которых нет в списке конъюнктивных групп параметров
    search_params_in_conjunctions = List.flatten(conjunctions)
    standalone_params = search_params
    |> Enum.reject(fn {field, _} -> String.to_atom(field) in search_params_in_conjunctions end)
    |> Enum.map(fn {field, _} -> [String.to_atom(field)] end)

    # Сначала сформируем дизъюнкцию конъюнкций, т.е. сочетаем через "или" группы полей, сочетающихся через "и"
    disjunction_of_conjunctions(queryable, conjunctions, search_params)
    # затем добавим к ней дизъюнкцию одиночных параметров, т.е. приклеим их через "или" по одному
    |> disjunction_of_conjunctions(standalone_params, search_params)
  end

  # Принимает:
  # - queryable,
  # - список групп полей, которые нужно соединить через "и",
  # - а также исходные поисковые параметры rummage.
  # Склеивает поля в каждой группе поля "и" (если, конечно, они есть в поисковом запросе),
  # затем полученное выражение приклеивает к queryable через "или",
  # и рекурсивно берет следующую группу из списка.
  defp disjunction_of_conjunctions(queryable, [], _), do: queryable

  defp disjunction_of_conjunctions(queryable, [fields | t], search_params) do
    # Сначала для каждого поля из fields построим динамическое выражение, соотв. поисковому запросу
    dyn_conditions = dynamic_field_conditions(fields, search_params)

    queryable = if Enum.all?(dyn_conditions, &(&1 == true)) do
      # Если для данной группы полей все выражения свелись к true, т.е. по ним реально ничего не ищут, отбросим их
      queryable
    else
      # Иначе - если ищут хотя бы по одному полю из группы - склеим их через "и",
      # а затем полученное выражение приклеим к queryable через "или".
      queryable
      |> or_where(^add_conjunction(dyn_conditions, dynamic([], true)))
    end

    disjunction_of_conjunctions(queryable, t, search_params)
  end

  # Рекурсивно склеивает заданные выражения через логическое "и",
  # используя для этого динамические запросы Ecto
  defp add_conjunction([], dynamic), do: dynamic
  defp add_conjunction([h | t], dynamic) do
    conjunction = dynamic([], ^dynamic and ^h)
    add_conjunction(t, conjunction)
  end

  # Принимает список полей,
  # возвращает список Ecto-условий, соотв. этим полям,
  # при этом условия обернуты в динамические запросы Ecto
  defp dynamic_field_conditions(fields, search_params) do
    fields
    |> Enum.map(fn field -> parse_field(field, search_params) end)
    |> Enum.map(&dynamic_condition/1)
  end

  defp parse_field(field, search_params) do
    field_as_string = field |> Atom.to_string()
    case search_params[field_as_string] do
      nil -> {nil, field, nil}
      params -> {params["search_type"], field, params["search_term"]}
    end
  end

  defp dynamic_condition({_, _, search_term}) when search_term in [nil, "", []], do: true

  defp dynamic_condition({"like", field, search_term}) do
    dynamic([s, field],
      like(field(s, ^field), ^"%#{String.replace(search_term, "%", "\\%")}%"))
  end

  defp dynamic_condition({"ilike", field, search_term}) do
    dynamic([s, field],
      ilike(field(s, ^field), ^"%#{String.replace(search_term, "%", "\\%")}%"))
  end

  defp dynamic_condition({"eq", field, search_term}) do
    dynamic([s, field],
      field(s, ^field) == ^search_term)
  end

  defp dynamic_condition({"gt", field, search_term}) do
    dynamic([s, field],
      field(s, ^field) > ^search_term)
  end

  defp dynamic_condition({"lt", field, search_term}) do
    dynamic([s, field],
      field(s, ^field) < ^search_term)
  end

  defp dynamic_condition({"gteq", field, search_term}) do
    dynamic([s, field],
      field(s, ^field) >= ^search_term)
  end

  defp dynamic_condition({"lteq", field, search_term}) do
    dynamic([s, field],
      field(s, ^field) <= ^search_term)
  end

  defp dynamic_condition({"in", field, search_term}) do
    dynamic([s, field],
      field(s, ^field) in ^search_term)
  end

end
